/*
 *      Copyright (C) 2005-2015 Team XBMC
 *      http://xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC Remote; see the file license.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */

package org.tinymediamanager.jsonrpc.notification;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Parses VideoLibrary.* events.
 * 
 * @author freezy <freezy@xbmc.org>
 */
public class VideoLibrary {

  /*
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * notifications:
   * https://github.com/xbmc/xbmc/blob/master/xbmc/interfaces/json-rpc/notifications.json * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * * * * * *
   */

  /**
   * Video library scan finished.
   */
  public static class OnScanFinished extends AbstractEvent {
    public final static int    ID     = 0x31;
    public final static String METHOD = "VideoLibrary.OnScanFinished";

    public OnScanFinished(ObjectNode node) {
      super(node);
    }

    @Override
    public String toString() {
      return "VIDEOSCAN FINISHED";
    }

    @Override
    public int getId() {
      return ID;
    }

    @Override
    public String getMethod() {
      return METHOD;
    }
  }
  /**
   * Video library scan started.
   */
  public static class OnScanStarted extends AbstractEvent {
    public final static int    ID     = 0x32;
    public final static String METHOD = "VideoLibrary.OnScanStarted";

    public OnScanStarted(ObjectNode node) {
      super(node);
    }

    @Override
    public String toString() {
      return "VIDEOSCAN STARTED";
    }

    @Override
    public int getId() {
      return ID;
    }

    @Override
    public String getMethod() {
      return METHOD;
    }
  }

}
